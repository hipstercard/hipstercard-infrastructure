#  🎡 Infrastructure Service

- This repository is responsible for setting up all the required infrastructure (PostgreSQL and RabbitMQ containers).
- Now it is done manually by activating the `deploy_infrastucture` job.

# ❗️ Need to be fixed

- Now every time after deploying infrastructure, I need to run `psql -h 45.153.71.217 -p 5432 -U hipstercard_user -d postgres -c "create database cards_db; create database transactions_db;"` command to create databases needed for microservices.
- This proccess should be automatic... (I don't know how to implement it)